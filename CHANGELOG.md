# v1.0.3
## Minor documentation improvements

Fixes to README.md, clarify things, etc.

# v1.0.0
## Initial release
This is the initial release. Included here is:
- pending, error, success async values
- Unpacking values with `resolve` and `get*`
- Combining values with `all` and `some`
- Transforming values with `map*` and `flatMap*`
- Checking state with `is*`
