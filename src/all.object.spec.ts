import { all } from "./all";
import { success, pending, error } from "./index";
import { UNSAFE_getValue, UNSAFE_getError } from "./test-utils/peek";

describe("all(obj)", () => {
  describe("Given an empty object", () => {
    const value = {};

    it("returns a resolved async value of an empty object", () => {
      const result = all(value);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toEqual({});
    });
  });

  describe("Given an object of successful async values", () => {
    const value = {
      a: success(3),
      b: success(4)
    };

    it("returns a resolved async value that resolves to an object, matching the input values", () => {
      const result = all(value);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toEqual({ a: 3, b: 4 });
    });
  });

  describe("Given an object of pending async values", () => {
    const value = {
      a: pending(),
      b: pending()
    };

    it("returns a pending async value", () => {
      const result = all(value);
      expect(result.isPending()).toBe(true);
    });
  });

  describe("Given an object of error async values", () => {
    const value = {
      a: error("An error"),
      b: error("Another error")
    };

    it("returns an error async value that resolves to an object of errors, matching the input values", () => {
      const result = all(value);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toEqual({
        a: "An error",
        b: "Another error"
      });
    });
  });

  describe("Given an object of successful and pending asyncvalues", () => {
    const value = {
      a: pending(),
      b: success("success")
    };

    it("returns a pending async value", () => {
      const result = all(value);
      expect(result.isPending()).toBe(true);
    });
  });

  describe("Given an object of successful and error asyncvalues", () => {
    const value = {
      a: success(3),
      b: error("An error")
    };

    it("Returns an error asyncvalue that resolves to an object, matching the input errors, using `undefined` for non-errors", () => {
      const result = all(value);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toEqual({ a: undefined, b: "An error" });
    });
  });

  describe("Given an object of error and pending asyncvalues", () => {
    const value = {
      a: pending(),
      b: error("An error")
    };

    it("Returns an error asyncvalue that resolves to an object, matching the input errors, using `undefined` for non-errors", () => {
      const result = all(value);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toEqual({ a: undefined, b: "An error" });
    });
  });

  describe("Given an object of error, successful and pending asyncvalues", () => {
    const value = {
      a: pending(),
      b: error("An error"),
      c: success("success")
    };

    it("Returns an error asyncvalue that resolves to an object, matching the input errors, using `undefined` for non-errors", () => {
      const result = all(value);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toEqual({
        a: undefined,
        b: "An error",
        c: undefined
      });
    });
  });
});
