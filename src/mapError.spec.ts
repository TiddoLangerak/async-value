import { pending, error, success } from "./index";
import { UNSAFE_getError, UNSAFE_getValue } from "./test-utils/peek";

describe("mapError()", () => {
  describe("Given a mapping function that returns a constant value", () => {
    const mappedResult = 3;
    const mapper = () => mappedResult;

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.mapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error("error");
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the mapped error", () => {
        const mapped = anErrorAsyncValue.mapError(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedResult);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns a success async value that resolves to the original value", () => {
        const mapped = aSuccessAsyncValue.mapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(value);
      });
    });
  });

  describe("Given a mapping function that throws", () => {
    const mappedError = new Error("err");
    const mapper = () => {
      throw mappedError;
    };

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.mapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the thrown error", () => {
        const mapped = anErrorAsyncValue.mapError(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns an error async value that resolves to the original value", () => {
        const mapped = aSuccessAsyncValue.mapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(value);
      });
    });
  });
});
