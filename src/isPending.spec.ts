import { pending, error, success } from "./index";

describe("isPending()", () => {
  describe("Given a pending async value", () => {
    const aPendingAsyncValue = pending();

    it("returns true", () => {
      expect(aPendingAsyncValue.isPending()).toBe(true);
    });
  });

  describe("Given an error async value", () => {
    const anErrorAsyncValue = error(1);

    it("returns false", () => {
      expect(anErrorAsyncValue.isPending()).toBe(false);
    });
  });

  describe("Given a success async value", () => {
    const aSuccessAsyncValue = success(1);

    it("returns false", () => {
      expect(aSuccessAsyncValue.isPending()).toBe(false);
    });
  });
});
