import { pending } from "./pending";
import { AsyncValue } from "./asyncValue";

// A pending value is assignable to any AsyncValue
pending() as AsyncValue<void>;
pending() as AsyncValue<number>;
pending() as AsyncValue<{}>;
pending() as AsyncValue<{ x: number; y: number }>;

// A pending value is NOT assignable to just any value
// $ExpectError
pending() as number;
// $ExpectError
pending() as { x: number };
