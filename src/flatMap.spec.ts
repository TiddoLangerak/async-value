import { pending, error, success } from "./index";
import { UNSAFE_getError, UNSAFE_getValue } from "./test-utils/peek";

describe("flatMap()", () => {
  describe("Given a mapping function that resolves to a pending value", () => {
    const mapper = () => pending();

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMap(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the original error", () => {
        const mapped = anErrorAsyncValue.flatMap(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(theError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns a pending async value", () => {
        const mapped = aSuccessAsyncValue.flatMap(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });
  });

  describe("Given a mapping function that resolves to an error value", () => {
    const mappedError = new Error("Error");
    const mapper = () => error(mappedError);

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMap(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the original error", () => {
        const mapped = anErrorAsyncValue.flatMap(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(theError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns an error async value that resolves with the mapped error", () => {
        const mapped = aSuccessAsyncValue.flatMap(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedError);
      });
    });
  });

  describe("Given a mapping function that resolves to a success value containing the string length", () => {
    const mapper = (val: string) => success(val.length);

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMap(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the original error", () => {
        const mapped = anErrorAsyncValue.flatMap(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(theError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const valueLength = value.length;
      const aSuccessAsyncValue = success(value);

      it("returns a success value that resolves with the mapped value", () => {
        const mapped = aSuccessAsyncValue.flatMap(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(valueLength);
      });
    });
  });

  describe("Given a mapping function that throws", () => {
    const mappedError = new Error("err");
    const mapper = () => {
      throw mappedError;
    };

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMap(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the original error", () => {
        const mapped = anErrorAsyncValue.flatMap(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(theError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns an error async value that resolves to the thrown error", () => {
        const mapped = aSuccessAsyncValue.flatMap(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedError);
      });
    });
  });
});
