import { AsyncValue } from "./asyncValue";
import { ArrayAsTuple } from "./util/tuple";
import { pending, success, error } from "./index";
import { mapValues } from "./util/obj";

type AsyncValueMapOrTuple<T> = ArrayAsTuple<
  { [P in keyof T]: AsyncValue<T[P]> }
>;

export function some<RESULT_SHAPE>(
  values: AsyncValueMapOrTuple<RESULT_SHAPE>
): AsyncValue<Partial<RESULT_SHAPE>> {
  if (Array.isArray(values)) {
    if (values.some(v => v.isSuccess())) {
      return success((values.map(v =>
        v.getOrDefault(undefined)
      ) as unknown) as RESULT_SHAPE);
    } else if (values.every(v => v.isError())) {
      return error((values.map(v =>
        v.getErrorOrGetDefault(() => {
          throw new Error("Invalid state");
        })
      ) as unknown) as RESULT_SHAPE);
    } else {
      return pending();
    }
  } else {
    const entries: Array<[string, AsyncValue<unknown>]> = Object.entries(
      values
    );
    if (entries.some(([_, val]) => val.isSuccess())) {
      return success(mapValues(values, val =>
        val.getOrDefault(undefined)
      ) as RESULT_SHAPE);
    } else if (entries.every(([_, val]) => val.isError())) {
      return error(
        mapValues(values, val =>
          val.getErrorOrGetDefault(() => {
            throw new Error("Invalid state");
          })
        )
      );
    } else {
      return pending();
    }
  }
}
