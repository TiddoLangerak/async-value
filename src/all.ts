import { AsyncValue } from "./asyncValue";
import { ArrayAsTuple } from "./util/tuple";
import { pending, error, success } from "./index";
import { mapValues } from "./util/obj";

type AsyncValueMapOrTuple<T> = ArrayAsTuple<
  { [P in keyof T]: AsyncValue<T[P]> }
>;

export function all<RESULT_SHAPE>(
  values: AsyncValueMapOrTuple<RESULT_SHAPE>
): AsyncValue<RESULT_SHAPE> {
  if (Array.isArray(values)) {
    if (values.some(v => v.isError())) {
      return error(values.map(v => v.getErrorOrDefault(undefined)));
    } else if (values.some(v => v.isPending())) {
      return pending();
    } else {
      const result = (values.map(v =>
        v.getOrGetDefault(() => {
          throw new Error("Invalid state");
        })
      ) as unknown) as RESULT_SHAPE;
      return success(result);
    }
  } else {
    const entries: Array<[string, AsyncValue<unknown>]> = Object.entries(
      values
    );
    if (entries.some(([_, val]) => val.isError())) {
      return error(mapValues(values, val => val.getErrorOrDefault(undefined)));
    } else if (entries.some(([_, val]) => val.isPending())) {
      return pending();
    } else {
      return success(mapValues(values, val =>
        val.getOrGetDefault(() => {
          throw new Error("Invalid state");
        })
      ) as RESULT_SHAPE);
    }
  }
}
