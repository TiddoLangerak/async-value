import { AsyncValue } from "./asyncValue";
import { error } from "./error";

/**
 * Constructs an async value representing a resolved value
 */
export function success<VALUE>(val: VALUE): AsyncValue<VALUE> {
  const self: AsyncValue<VALUE> = {
    isPending: () => false,
    isSuccess: () => true,
    isError: () => false,

    resolve: resolver => resolver.success(val),

    map: mapper => {
      try {
        return success(mapper(val));
      } catch (e) {
        return error(e);
      }
    },
    flatMap: mapper => {
      try {
        return mapper(val);
      } catch (e) {
        return error(e);
      }
    },
    mapError: () => self,
    flatMapError: () => self,

    getOrDefault: () => val,
    getOrGetDefault: () => val,
    getErrorOrDefault: def => def,
    getErrorOrGetDefault: getDefault => getDefault()
  };

  return self;
}
