export interface Resolver<VALUE, RESULT> {
  success(value: VALUE): RESULT;
  error(error: unknown): RESULT;
  pending(): RESULT;
}

export interface AsyncValue<VALUE> {
  isPending(): boolean;
  isSuccess(): boolean;
  isError(): boolean;

  resolve<RESULT>(resolver: Resolver<VALUE, RESULT>): RESULT;

  map<RESULT>(mapper: (value: VALUE) => RESULT): AsyncValue<RESULT>;
  flatMap<RESULT>(
    mapper: (value: VALUE) => AsyncValue<RESULT>
  ): AsyncValue<RESULT>;
  mapError(mapper: (error: unknown) => unknown): AsyncValue<VALUE>;
  flatMapError<RESULT>(
    mapper: (error: unknown) => AsyncValue<RESULT>
  ): AsyncValue<RESULT | VALUE>;

  getOrDefault<DEFAULT>(def: DEFAULT): VALUE | DEFAULT;
  getOrGetDefault<DEFAULT>(getDefault: () => DEFAULT): VALUE | DEFAULT;
  getErrorOrDefault(def: unknown): unknown;
  getErrorOrGetDefault(getDefault: () => unknown): unknown;
}
