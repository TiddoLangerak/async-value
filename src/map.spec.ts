import { pending, error, success } from "./index";
import { UNSAFE_getError, UNSAFE_getValue } from "./test-utils/peek";

describe("map()", () => {
  describe("Given a mapping function 'stringLength(string): number'", () => {
    const stringLength = (str: String) => str.length;

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.map(stringLength);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the original error", () => {
        const mapped = anErrorAsyncValue.map(stringLength);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(theError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const valueLength = value.length;
      const aSuccessAsyncValue = success(value);

      it("returns a success async value that resolves to the mapped value", () => {
        const mapped = aSuccessAsyncValue.map(stringLength);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(valueLength);
      });
    });
  });

  describe("Given a mapping function that throws", () => {
    const mappedError = new Error("err");
    const mapper = () => {
      throw mappedError;
    };

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.map(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the original error", () => {
        const mapped = anErrorAsyncValue.map(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(theError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns an error async value that resolves to the thrown error", () => {
        const mapped = aSuccessAsyncValue.map(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedError);
      });
    });
  });
});
