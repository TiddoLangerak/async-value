import { AsyncValue } from "./asyncValue";

const _pending: AsyncValue<never> = {
  isPending: () => true,
  isSuccess: () => false,
  isError: () => false,

  resolve: resolver => resolver.pending(),

  map: () => _pending,
  flatMap: () => _pending,
  mapError: () => _pending,
  flatMapError: () => _pending,

  getOrDefault: def => def,
  getOrGetDefault: getDefault => getDefault(),
  getErrorOrDefault: def => def,
  getErrorOrGetDefault: getDefault => getDefault()
};

export const pending = () => _pending;
