import { pending, error, success } from "./index";

describe("isSuccess()", () => {
  describe("Given a pending async value", () => {
    const aPendingAsyncValue = pending();

    it("returns false", () => {
      expect(aPendingAsyncValue.isSuccess()).toBe(false);
    });
  });

  describe("Given an error async value", () => {
    const anErrorAsyncValue = error(1);

    it("returns false", () => {
      expect(anErrorAsyncValue.isSuccess()).toBe(false);
    });
  });

  describe("Given a success async value", () => {
    const aSuccessAsyncValue = success(1);

    it("returns true", () => {
      expect(aSuccessAsyncValue.isSuccess()).toBe(true);
    });
  });
});
