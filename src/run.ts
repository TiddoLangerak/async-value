import { AsyncValue, success, pending, error } from "./index";

type Bind = <V>(v: AsyncValue<V>) => V;
export type Runner<R> = (bind: Bind) => R;

class ShortCircuitBind<T> {
  result: AsyncValue<T>;
  constructor(result: AsyncValue<T>) {
    this.result = result;
  }
}

function bind<V>(v: AsyncValue<V>): V {
  return v.resolve({
    success: x => x,
    pending: () => {
      throw new ShortCircuitBind(pending());
    },
    error: err => {
      throw new ShortCircuitBind(error(err));
    }
  });
}

export function run<T>(func: Runner<T>): AsyncValue<T> {
  try {
    return success(func(bind));
  } catch (e) {
    if (e instanceof ShortCircuitBind) {
      return e.result;
    }
    throw e;
  }
}
