import { pending, error, success } from "./index";

describe("getErrorOrDefault()", () => {
  describe("Given a success asyncvalue", () => {
    const value = success(3);

    it("returns the default", () => {
      expect(value.getErrorOrDefault(1)).toBe(1);
    });
  });

  describe("Given a pending asyncvalue", () => {
    const value = pending();
    it("returns the default", () => {
      expect(value.getErrorOrDefault(1)).toBe(1);
    });
  });

  describe("Given an error asyncvalue", () => {
    const value = error("error");
    it("returns the error", () => {
      expect(value.getErrorOrDefault(1)).toBe("error");
    });
  });
});
