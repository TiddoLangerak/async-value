import { AsyncValue } from "../asyncValue";

export function UNSAFE_getError<T>(value: AsyncValue<T>): unknown {
  return value.resolve({
    error: err => err,
    pending: () => {
      throw new Error("Cannot get error value from a pending async value");
    },
    success: () => {
      throw new Error("Cannot get error value from a success async value");
    }
  });
}

export function UNSAFE_getValue<T>(value: AsyncValue<T>): T {
  return value.resolve({
    error: () => {
      throw new Error("Cannot get error value from an error value");
    },
    pending: () => {
      throw new Error("Cannot get error value from a pending async value");
    },
    success: val => val
  });
}
