import { pending, error, success } from "./index";

describe("getOrGetDefault()", () => {
  describe("Given a success asyncvalue", () => {
    const value = success(3);

    it("returns the value", () => {
      expect(value.getOrGetDefault(jest.fn())).toBe(3);
    });

    it("does not invoke the getDefault function", () => {
      const getDefault = jest.fn();
      value.getOrGetDefault(getDefault);
      expect(getDefault).not.toHaveBeenCalled();
    });
  });

  describe("Given a pending asyncvalue", () => {
    const value = pending();
    describe("Given a getDefault function that returns a value", () => {
      const getDefault = () => 1;

      it("returns the value returned by getDefault", () => {
        expect(value.getOrGetDefault(getDefault)).toBe(1);
      });
    });

    describe("Given a getDefault function that throws", () => {
      const error = new Error("error");
      const getDefault = () => {
        throw error;
      };

      it("rethrows the error thrown by getDefault", () => {
        expect(() => value.getOrGetDefault(getDefault)).toThrow(error);
      });
    });
  });

  describe("Given an error asyncvalue", () => {
    const value = error("error");
    describe("Given a getDefault function that returns a value", () => {
      const getDefault = () => 1;

      it("returns the value returned by getDefault", () => {
        expect(value.getOrGetDefault(getDefault)).toBe(1);
      });
    });

    describe("Given a getDefault function that throws", () => {
      const error = new Error("error");
      const getDefault = () => {
        throw error;
      };

      it("rethrows the error thrown by getDefault", () => {
        expect(() => value.getOrGetDefault(getDefault)).toThrow(error);
      });
    });
  });
});
