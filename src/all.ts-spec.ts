import { all } from "./all";
import { success } from "./success";

// Does not accept primitives
// $ExpectError
all(3);
// $ExpectError
all("test");
// $ExpectError
all(false);

// Does not accept objects with non-asyncvalues
// $ExpectError
all({ x: 3 });
// $ExpectError
all({ x: success(3), y: 4 });

// Does not accept arrays with non-asyncvalues
// $ExpectError
all([3]);
// $ExpectError
all([3, success(3)]);

// Correctly maps object types
// $ExpectType AsyncValue<{ x: number; y: string; }>
all({ x: success(3), y: success("hello") });

// Correctly maps arrays as tuples
// $ExpectType AsyncValue<[number, string]>
all([success(3), success("hello")]);
