import { some } from "./some";
import { success } from "./success";

// Does not accept primitives
// $ExpectError
some(3);
// $ExpectError
some("test");
// $ExpectError
some(false);

// Does not accept objects with non-asyncvalues
// $ExpectError
some({ x: 3 });
// $ExpectError
some({ x: success(3), y: 4 });

// Does not accept arrays with non-asyncvalues
// $ExpectError
some([3]);
// $ExpectError
some([3, success(3)]);

// Correctly maps object types
// $ExpectType AsyncValue<Partial<{ x: number; y: string; }>>
some({ x: success(3), y: success("hello") });

// Correctly maps arrays as tuples
// $ExpectType AsyncValue<[(number | undefined)?, (string | undefined)?]>
some([success(3), success("hello")]);
