import { mapValues } from "./obj";

describe("mapValues", () => {
  describe("Given an empty object", () => {
    const values = {};
    it("returns an empty object", () => {
      expect(mapValues(values, x => x)).toEqual({});
    });
  });

  describe("Given an object of strings, and a str.length mapper", () => {
    const values = { a: "a", b: "aaa", c: "aa" };
    const mapper = (x: string) => x.length;
    it("returns an object of string lengths", () => {
      expect(mapValues(values, mapper)).toEqual({ a: 1, b: 3, c: 2 });
    });
  });

  describe("Given a non-empty object and a mapper that throws", () => {
    const values = { a: "x" };
    const mapper = () => {
      throw new Error("test");
    };
    it("should rethrow the error", () => {
      expect(() => mapValues(values, mapper)).toThrowError(/^test$/);
    });
  });
});
