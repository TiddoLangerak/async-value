import { arrayAsTuple } from "./tuple";

// $ExpectType [number, string]
arrayAsTuple([1, "hello"]);
// $ExpectType [string, string]
arrayAsTuple(["hello", "world"]);
// $ExpectType "test"
arrayAsTuple("test");
// $ExpectType { x: number; }
arrayAsTuple({ x: 3 });
// $ExpectType string[]
arrayAsTuple(["hello", "world"] as string[]);
