import { mapValues } from "./obj";
// shouldn't take a number as argument
// $ExpectError
mapValues(3, () => undefined);

// should take an empty object as argument
mapValues({}, () => undefined);

// should return an identical object type if the mapper is the identity function
// $ExpectType { a: number; b: number; }
mapValues({ a: 3, b: 4 }, x => x);

// only deals with uniform objects (TODO: can we improve this?)
// $ExpectType { a: string | number; b: string | number; }
mapValues({ a: 3, b: "asdf" }, x => x);

// should infer the return type based on the mapping function
// $ExpectType { a: string; b: string; }
mapValues({ a: 3, b: 5 }, () => "test");
