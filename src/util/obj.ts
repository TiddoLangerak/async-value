export function mapValues<O extends object, R>(
  values: O,
  mapper: (val: O[keyof O]) => R
): { [P in keyof O]: R } {
  return Object.entries(values)
    .map(([key, val]) => [key, mapper(val)] as [keyof O, R])
    .reduce(
      (obj, [key, val]) => {
        obj[key] = val;
        return obj;
      },
      {} as { [P in keyof O]: R }
    );
}
