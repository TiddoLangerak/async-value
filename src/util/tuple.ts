/**
 * Ensures a type is a tuple or object.
 *
 * TypeScript infers array litterals as arrays, not as tuples. e.g.:
 * [1, 'string'] -> (number | string)[]
 *
 * This behaviour can also be observed in generic functions:
 *
 * function id<T>(val: T) : T {
 *   return val;
 * }
 *
 * id([1, 'string']) -> (number | string)[]
 *
 * When creating wrapped types this is often not desireable. E.g. consider a function that takes a tuple and wraps
 * each element into an array. e.g. wrap([1, 'string']) = [[1], ['string']]. We can use mapped types to represent
 * the return value, like this:
 *
 * type Wrapped<T> = {
 *   [P in keyof T]: T[P][]
 * }
 *
 * Wrapped<[number, string]> -> Wrapped<[number[], string[]]>
 *
 * This looks exactly what we want! However, if we no try to use it in the function signature, then we're not so lucky:
 *
 * function wrap<T>(arr: T) : Wrapped<T> { ... }
 *
 * wrap([3, 'foo']) -> [(number | string)[]]
 *
 * Typescript infers an array with an intersection type here, instead of an array.
 *
 * The `MapOrTuple` type solves this by forcing typescript to infer the argument as a tuple instead:
 *
 * function wrap<T>(arr: ArrayAsTuple<T>): Wrapped<T> { ... }
 *
 * wrap([3, 'foo']) -> [number, string]
 */

export type ArrayAsTuple<T> = T & ([T[keyof T]] | {});
export function arrayAsTuple<T>(val: ArrayAsTuple<T>): T {
  return val;
}
