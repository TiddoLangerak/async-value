import { pending, error, success } from "./index";

describe("resolve()", () => {
  describe("Given a resolver with unique values returned in each branch", () => {
    const resolvedForPending = { resolvedFor: "pending" };
    const resolvedForError = { resolvedFor: "error" };
    const resolvedForSuccess = { resolvedFor: "success" };

    const resolver = {
      pending: jest.fn(() => resolvedForPending),
      error: jest.fn(() => resolvedForError),
      success: jest.fn(() => resolvedForSuccess)
    };

    afterEach(() => {
      resolver.pending.mockClear();
      resolver.error.mockClear();
      resolver.success.mockClear();
    });

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns the result of Resolver.pending", () => {
        expect(aPendingAsyncValue.resolve(resolver)).toBe(resolvedForPending);
      });
    });

    describe("Given an error async value", () => {
      const err = new Error("Test error");
      const anErrorAsyncValue = error(err);

      it("returns the result of Resolver.error", () => {
        expect(anErrorAsyncValue.resolve(resolver)).toBe(resolvedForError);
      });

      it("passes the error to Resolver.error", () => {
        anErrorAsyncValue.resolve(resolver);
        expect(resolver.error).toHaveBeenCalledWith(err);
      });
    });

    describe("Given a success async value", () => {
      const value = "a value";
      const aSuccessAsyncValue = success(value);

      it("returns the result of Resolver.success", () => {
        expect(aSuccessAsyncValue.resolve(resolver)).toBe(resolvedForSuccess);
      });

      it("returns pass the value to Resolver.success", () => {
        aSuccessAsyncValue.resolve(resolver);
        expect(resolver.success).toHaveBeenCalledWith(value);
      });
    });
  });

  describe("Given a resolver that throws", () => {
    const pendingError = new Error("pending error");
    const successError = new Error("success error");
    const errorError = new Error("error error");

    const resolver = {
      pending: () => {
        throw pendingError;
      },
      success: () => {
        throw successError;
      },
      error: () => {
        throw errorError;
      }
    };

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("throws the pending error", () => {
        expect(() => aPendingAsyncValue.resolve(resolver)).toThrowError(
          pendingError
        );
      });
    });

    describe("Given an error async value", () => {
      const err = new Error("Test error");
      const anErrorAsyncValue = error(err);

      it("throws the error error", () => {
        expect(() => anErrorAsyncValue.resolve(resolver)).toThrowError(
          errorError
        );
      });
    });

    describe("Given a success async value", () => {
      const value = "a value";
      const aSuccessAsyncValue = success(value);

      it("throws the success error", () => {
        expect(() => aSuccessAsyncValue.resolve(resolver)).toThrowError(
          successError
        );
      });
    });
  });
});
