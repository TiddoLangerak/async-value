import { error } from "./error";
import { AsyncValue } from "./asyncValue";

// An error value is assignable to any AsyncValue
error(3) as AsyncValue<void>;
error(3) as AsyncValue<number>;
error(3) as AsyncValue<{}>;
error(3) as AsyncValue<{ x: number; y: number }>;

// A error value is NOT assignable to just any value
// $ExpectError
error(3) as number;
// $ExpectError
error(3) as { x: number };
