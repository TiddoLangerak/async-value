import { pending, error, success } from "./index";
import { UNSAFE_getError, UNSAFE_getValue } from "./test-utils/peek";

describe("flatMapError()", () => {
  describe("Given a mapping function that resolves to a pending value", () => {
    const mapper = () => pending();

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns a pending value", () => {
        const mapped = anErrorAsyncValue.flatMapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns a success async value that resolves to the original value", () => {
        const mapped = aSuccessAsyncValue.flatMapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(value);
      });
    });
  });

  describe("Given a mapping function that resolves to an error value", () => {
    const mappedError = new Error("Error");
    const mapper = () => error(mappedError);

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the mapped error", () => {
        const mapped = anErrorAsyncValue.flatMapError(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns a success async value that resolves to the original value", () => {
        const mapped = aSuccessAsyncValue.flatMapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(value);
      });
    });
  });

  describe("Given a mapping function that resolves to a success value containing a constant value", () => {
    const mappedValue = "hello";
    const mapper = () => success(mappedValue);

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns a success async value that resolves to the mapped value", () => {
        const mapped = anErrorAsyncValue.flatMapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(mappedValue);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns a success value that resolves with the original value", () => {
        const mapped = aSuccessAsyncValue.flatMapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(value);
      });
    });
  });

  describe("Given a mapping function that throws", () => {
    const mappedError = new Error("err");
    const mapper = () => {
      throw mappedError;
    };

    describe("Given a pending async value", () => {
      const aPendingAsyncValue = pending();

      it("returns a pending async value", () => {
        const mapped = aPendingAsyncValue.flatMapError(mapper);
        expect(mapped.isPending()).toBe(true);
      });
    });

    describe("Given an error async value", () => {
      const theError = new Error();
      const anErrorAsyncValue = error(theError);

      it("returns an error async value that resolves with the thrown error", () => {
        const mapped = anErrorAsyncValue.flatMapError(mapper);
        expect(mapped.isError()).toBe(true);
        expect(UNSAFE_getError(mapped)).toBe(mappedError);
      });
    });

    describe("Given a success async value", () => {
      const value = "a_string";
      const aSuccessAsyncValue = success(value);

      it("returns an async value with the original value", () => {
        const mapped = aSuccessAsyncValue.flatMapError(mapper);
        expect(mapped.isSuccess()).toBe(true);
        expect(UNSAFE_getValue(mapped)).toBe(value);
      });
    });
  });
});
