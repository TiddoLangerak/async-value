import { pending, error, success } from "./index";

describe("getOrDefault()", () => {
  describe("Given a success asyncvalue", () => {
    const value = success(3);

    it("returns the value", () => {
      expect(value.getOrDefault(1)).toBe(3);
    });
  });

  describe("Given a pending asyncvalue", () => {
    const value = pending();
    it("returns the default", () => {
      expect(value.getOrDefault(1)).toBe(1);
    });
  });

  describe("Given an error asyncvalue", () => {
    const value = error("error");
    it("returns the default", () => {
      expect(value.getOrDefault(1)).toBe(1);
    });
  });
});
