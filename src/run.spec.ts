import { run, Runner } from "./run";
import { pending, success, error } from "./index";
import { UNSAFE_getValue, UNSAFE_getError } from "./test-utils/peek";

describe("run()", () => {
  describe("Given an empty runner", () => {
    const runner: Runner<void> = () => {};

    it("returns a successful asyncvalue, resolving to `undefined`", () => {
      const result = run(runner);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toBeUndefined();
    });
  });

  describe("Given a runner that returns a value", () => {
    const runner: Runner<number> = () => 3;

    it("returns a successful asyncvalue, resolving to the runners value", () => {
      const result = run(runner);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toBe(3);
    });
  });

  describe("Given a runner that binds a pending value", () => {
    const afterPending = jest.fn();
    const runner: Runner<void> = bind => {
      bind(pending());
      afterPending();
    };

    it("returns a pending asyncvalue", () => {
      const result = run(runner);
      expect(result.isPending()).toBe(true);
    });

    it("does not execute any code after binding the pending value", () => {
      expect(afterPending).not.toHaveBeenCalled();
    });
  });

  describe("Given a runner that binds a success asyncvalue, and returns it", () => {
    const runner: Runner<number> = bind => {
      const val = bind(success(3));
      return val;
    };

    it("returns a success asyncvalue with the bound value", () => {
      const result = run(runner);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toBe(3);
    });
  });

  describe("Given a runner that binds 2 success asyncvalues and returns their sum", () => {
    const runner: Runner<number> = bind => {
      const a = bind(success(3));
      const b = bind(success(5));
      return a + b;
    };

    it("returns a success asyncvalue with the sum of the values", () => {
      const result = run(runner);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toBe(8);
    });
  });

  describe("Given a runner that binds an error asyncvalue", () => {
    const afterError = jest.fn();
    const runner: Runner<void> = bind => {
      bind(error("The error"));
      afterError();
    };

    it("returns an error asyncvalue that resolves to the error", () => {
      const result = run(runner);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toBe("The error");
    });

    it("does not execute any code after binding the error", () => {
      expect(afterError).not.toHaveBeenCalled();
    });
  });

  describe("Given a runner that throws", () => {
    const runner: Runner<unknown> = () => {
      throw new Error("x");
    };

    it("rethrows the error", () => {
      expect(() => run(runner)).toThrowError(/^x$/);
    });
  });
});
