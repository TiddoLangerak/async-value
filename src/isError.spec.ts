import { pending, error, success } from "./index";

describe("isError()", () => {
  describe("Given a pending async value", () => {
    const aPendingAsyncValue = pending();

    it("returns false", () => {
      expect(aPendingAsyncValue.isError()).toBe(false);
    });
  });

  describe("Given an error async value", () => {
    const anErrorAsyncValue = error(1);

    it("returns true", () => {
      expect(anErrorAsyncValue.isError()).toBe(true);
    });
  });

  describe("Given a success async value", () => {
    const aSuccessAsyncValue = success(1);

    it("returns false", () => {
      expect(aSuccessAsyncValue.isError()).toBe(false);
    });
  });
});
