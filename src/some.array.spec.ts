import { some } from "./some";
import { success, pending, error, AsyncValue } from "./index";
import { UNSAFE_getValue, UNSAFE_getError } from "./test-utils/peek";

describe("some(arr)", () => {
  describe("Given an empty array", () => {
    const values: AsyncValue<unknown>[] = [];

    it("returns an error asyncvalue with an empty payload", () => {
      const result = some(values);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toEqual([]);
    });
  });

  describe("Given an array of successful asyncvalues", () => {
    const values = [success(3), success("test"), success(true)];

    it("returns a successful asyncvalue that resolves to an array with the results of the individual asyncvalues", () => {
      const result = some(values);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toEqual([3, "test", true]);
    });
  });

  describe("Given an array of pending asyncvalues", () => {
    const values = [pending(), pending()];

    it("returns a pending asyncvalue", () => {
      const result = some(values);
      expect(result.isPending()).toBe(true);
    });
  });

  describe("Given an array of error asyncvalues", () => {
    const values = [error("error1"), error("error2")];

    it("returns an error asyncvalue that resolves with an array of errors, matching the input values", () => {
      const result = some(values);
      expect(result.isError()).toBe(true);
      expect(UNSAFE_getError(result)).toEqual(["error1", "error2"]);
    });
  });

  describe("Given an array of successful and pending asyncvalues", () => {
    const values = [success(3), success("test"), pending()];

    it("returns a success asyncvalue that resolves with an array of values, matching the input values, using `undefined` for non-values", () => {
      const result = some(values);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toEqual([3, "test", undefined]);
    });
  });

  describe("Given an array of successful and error asyncvalues", () => {
    const values = [success(3), error("error")];

    it("returns an success asyncvalue that resolves with an array of values, matching the input values, using `undefined` for non-values", () => {
      const result = some(values);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toEqual([3, undefined]);
    });
  });

  describe("Given an array of error and pending asyncvalues", () => {
    const values = [pending(), error("error")];

    it("returns a pending asyncvalue", () => {
      const result = some(values);
      expect(result.isPending()).toBe(true);
    });
  });

  describe("Given an array of error, successful and pending asynvalues", () => {
    const values = [pending(), error("error"), success(3)];

    it("returns an success asyncvalue that resolves with an array of values, matching the input values, using `undefined` for non-values", () => {
      const result = some(values);
      expect(result.isSuccess()).toBe(true);
      expect(UNSAFE_getValue(result)).toEqual([undefined, undefined, 3]);
    });
  });
});
