import { pending, error, success } from "./index";

describe("getErrorGetDefault()", () => {
  describe("Given an error asyncvalue", () => {
    const theError = new Error("error");
    const value = error(theError);

    it("returns the error", () => {
      expect(value.getErrorOrGetDefault(jest.fn())).toBe(theError);
    });

    it("does not invoke the getDefault function", () => {
      const getDefault = jest.fn();
      value.getErrorOrGetDefault(getDefault);
      expect(getDefault).not.toHaveBeenCalled();
    });
  });

  describe("Given a pending asyncvalue", () => {
    const value = pending();
    describe("Given a getDefault function that returns a value", () => {
      const getDefault = () => 1;

      it("returns the value returned by getDefault", () => {
        expect(value.getErrorOrGetDefault(getDefault)).toBe(1);
      });
    });

    describe("Given a getDefault function that throws", () => {
      const error = new Error("error");
      const getDefault = () => {
        throw error;
      };

      it("rethrows the error thrown by getDefault", () => {
        expect(() => value.getErrorOrGetDefault(getDefault)).toThrow(error);
      });
    });
  });

  describe("Given a success asyncvalue", () => {
    const value = success(3);
    describe("Given a getDefault function that returns a value", () => {
      const getDefault = () => 1;

      it("returns the value returned by getDefault", () => {
        expect(value.getErrorOrGetDefault(getDefault)).toBe(1);
      });
    });

    describe("Given a getDefault function that throws", () => {
      const error = new Error("error");
      const getDefault = () => {
        throw error;
      };

      it("rethrows the error thrown by getDefault", () => {
        expect(() => value.getErrorOrGetDefault(getDefault)).toThrow(error);
      });
    });
  });
});
