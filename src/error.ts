import { AsyncValue } from "./asyncValue";

export function error(err: unknown): AsyncValue<never> {
  const self: AsyncValue<never> = {
    isPending: () => false,
    isSuccess: () => false,
    isError: () => true,

    resolve: resolver => resolver.error(err),

    map: () => self,
    flatMap: () => self,
    mapError: mapper => {
      try {
        return error(mapper(err));
      } catch (e) {
        return error(e);
      }
    },
    flatMapError: mapper => {
      try {
        return mapper(err);
      } catch (e) {
        return error(e);
      }
    },

    getOrDefault: def => def,
    getOrGetDefault: getDefault => getDefault(),
    getErrorOrDefault: () => err,
    getErrorOrGetDefault: () => err
  };

  return self;
}
